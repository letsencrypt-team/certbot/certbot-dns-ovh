dns-lexicon>=3.15.1
setuptools>=41.6.0
acme>=2.9.0
certbot>=2.9.0

[docs]
Sphinx>=1.0
sphinx_rtd_theme

[test]
pytest
